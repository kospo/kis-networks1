import random
import time


class Settings:
    STATION_IDLE_TO_READY_CHANCE = 0.30
    TIME_STEP = 51.2 * 10 ** -6
    N = 3


class Network(object):
    TICK_SLEEP_TIME_SEC = Settings.TIME_STEP

    def __init__(self, stations, time_step):
        super(Network, self).__init__()
        self.signal = set()
        self.clock = 0
        self.time_step = time_step
        self.stations = stations

    @property
    def the_time(self):
        return self.clock * self.time_step

    def tick(self):
        # print('[%f] the signal is %s' % (self.the_time, self.signal))

        ended_frames = set(f for f in self.signal if f.propagate())

        for s in self.stations:
            s.tick()
        # if ended_frames:
        #     print('[%f] RM ended_frames=%s' % (self.the_time, ended_frames))
        self.clock += 1
        self.signal -= ended_frames
        time.sleep(self.TICK_SLEEP_TIME_SEC)

    def add_station(self, s):
        self.stations.add(s)
        s.network = self


class Frame(object):
    FRAME_LEN_TICKS = 23.71875

    def __init__(self, id, station):
        super(Frame, self).__init__()
        self.remaining_len = self.FRAME_LEN_TICKS
        self.id = id
        self.station = station

    def propagate(self):
        self.remaining_len -= 1
        return self.remaining_len <= 0

    def is_on_wire(self):
        """== has not ended yet & is being transmitted"""
        return 0 <= self.remaining_len < self.FRAME_LEN_TICKS

    def __str__(self):
        return 'Frame{id=%d, station=%s, rem_len=%d, on_wire=%r}' % (self.id, self.station, self.remaining_len, self.is_on_wire())

    __repr__ = __str__


class StationState(object):
    IDLE = 1
    READY_TO_TRANSMIT = 2
    TRANSMITTING = 3
    WAITING_FOR_LINE_TO_FREE = 4

    @classmethod
    def from_int(cls, state):
        for name, value in cls.__dict__.items():
            if not name.startswith('_') and\
                    name == name.upper() and\
                    value == state:
                return name
        raise ValueError


class Station(object):
    MAX_WAIT_TICKS = 10
    _id_counter = 0

    def __init__(self, idle_to_ready_chance=None, network=None):
        super(Station, self).__init__()
        self.__class__._id_counter += 1
        self.id = self._id_counter
        self.frame_counter = 0
        self.network = network
        self.state = StationState.IDLE
        self.idle_to_ready_chance = idle_to_ready_chance
        self._clocks = {
            StationState.TRANSMITTING: 0,
            StationState.WAITING_FOR_LINE_TO_FREE: 0,
        }
        self.last_frame_sent = None

    def tick(self):
        # station may have changed state at [clock-1, clock)
        self.pre_transmit_work()

        # if has data to transmit: start transmit OR start waiting for line to free
        if self.state == StationState.READY_TO_TRANSMIT:
            frames_on_wire = [f for f in self.network.signal if f.is_on_wire()]
            # there are no measurable frames: transmit!
            if len(frames_on_wire) == 0:
                # print('[%f] %d starting to transmit' % (self.network.the_time, self.id))
                self.state = StationState.TRANSMITTING
                self._clocks[self.state] = Frame.FRAME_LEN_TICKS
                new_frame = Frame(self.frame_counter, self)
                self.network.signal |= {new_frame}
                self.last_frame_sent = self.network.the_time
            else:
                # print('[%f] %d wants to transmit, but the network is busy, waiting..' % (self.network.the_time, self.id))
                self.start_waiting()

        # if transmitting: proceed OR end OR detect collision
        elif self.state == StationState.TRANSMITTING:
            other_frames_on_wire = [f for f in self.network.signal
                                    if f.is_on_wire() and f.station != self]
            if len(other_frames_on_wire) > 0:
                # collision detected, need to wait & resend.
                # print('[%f] %d was transmitting, but discovered collision, waiting..' % (self.network.the_time, self.id))
                self.start_waiting()
            else:
                self._clocks[self.state] -= 1
                if self._clocks[self.state] <= 0:
                    # print('[%f] %d finished transmitting OK' % (self.network.the_time, self.id))
                    # print('Station %d successfully transmitted a frame. Transmission was started at %f' % (self.id, self.last_frame_sent))
                    print('%.8f' % self.last_frame_sent)
                    self.state = StationState.IDLE
                    self.frame_counter += 1

    def pre_transmit_work(self):
        # the station may have some data to transmit after the previous time period
        if self.state == StationState.IDLE:
            if random.random() < self.idle_to_ready_chance:
                # print('[%f] %d has data to transmit' % (self.network.the_time, self.id))
                self.state = StationState.READY_TO_TRANSMIT
        # the station may have ended waiting after the previous time period
        if self.state == StationState.WAITING_FOR_LINE_TO_FREE:
            self._clocks[self.state] -= 1
            if self._clocks[self.state] <= 0:
                # print('[%f] %d finished waiting' % (self.network.the_time, self.id))
                self.state = StationState.READY_TO_TRANSMIT

    def start_waiting(self):
        self.state = StationState.WAITING_FOR_LINE_TO_FREE
        self._clocks[StationState.WAITING_FOR_LINE_TO_FREE] = random.randint(0, self.MAX_WAIT_TICKS)

    def __str__(self):
        return 'Station{id=%d, state=%s, clocks=%s}' % (self.id, StationState.from_int(self.state), self._clocks)

    __repr__ = __str__


if __name__ == '__main__':
    network = Network(set(), Settings.TIME_STEP)
    for i in range(Settings.N):
        network.add_station(Station(idle_to_ready_chance=Settings.STATION_IDLE_TO_READY_CHANCE))
    print('--- N=%d ; MAX_WAIT_TICKS=%d; STATION_IDLE_TO_READY_CHANCE=%f ---' % (Settings.N, Station.MAX_WAIT_TICKS,
                                                                                 Settings.STATION_IDLE_TO_READY_CHANCE))
    while True:
        try:
            network.tick()
        except KeyboardInterrupt:
            # print('got ^C')
            # print('stations=', network.stations)
            # print('signal=', network.signal)
            exit(0)
